from rules.kraken import deal


def demo():
    hands = deal()
    for i, hand in enumerate(hands, start=1):
        print(f'Hand {i}: {hand}')


if __name__ == '__main__':
    demo()
