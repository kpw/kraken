# -*- coding: utf-8 -*-
"""
A game has four players (two teams of two players sitting opposite).
A game is played in hands.
A hand is the play from the time the cards are dealt until they are redealt.
A hand commences with the dealer shuffling and dealing the cards.
Then the first player after the dealer declares the trump suit.
This player is the declarer.
Eight tricks are played following Klaverjas rules.
Points are counted for the two teams.
Unless the opponents scored more points than the declarer's team, points
are awarded to each team. Otherwise, the points are summed and awarded
to the opponents team with none going to the declarer's team.
"""
from typing import List, NamedTuple


SUITS = CLUBS, DIAMONDS, SPADES, HEARTS \
      = (0, 1, 2, 3)
RANKS = SEVEN, EIGHT, NINE, JACK, QUEEN, KING, TEN, ACE \
      = (0, 1, 2, 3, 4, 5, 6, 7)

TRUMP_RANKS = (0, 1, 6, 7, 2, 3, 4, 5)
CARD_VALUES = (0, 0, 0, 2, 3, 4, 10, 11)
CARD_VALUES_TRUMP = (0, 0, 14, 20, 2, 3, 10, 11)

US = MATE = 0
THEM = 1

BONUS_SERIES_TABLE = [
    set(RANKS[start:start+length])
    for length in range(3, 5)
    for start in range(len(RANKS) + 1 - length)
]
BONUS_SERIES_HAND = BONUS_SERIES_TABLE + [
    set(RANKS[start:start+length])
    for length in range(5, 9)
    for start in range(len(RANKS) + 1 - length)
]


class Card(NamedTuple):
    suit: int
    rank: int

    def __str__(self) -> str:
        suit_labels = ('♣', '♦', '♠', '♥')
        rank_labels = ('7', '8', '9', 'J', 'Q', 'K', '10', 'A')
        return f'{suit_labels[self.suit]}{rank_labels[self.rank]}'

    @property
    def trump_rank(self) -> int:
        return TRUMP_RANKS[self.rank]

    @property
    def trump_value(self) -> int:
        return CARD_VALUES_TRUMP[self.rank]

    @property
    def value(self) -> int:
        return CARD_VALUES[self.rank]


ALL_CARDS = tuple(Card(s, r) for s in SUITS for r in RANKS)


class Hand(List[Card]):
    def __init__(self, *args, **kwargs) -> None:
        l = list(*args, **kwargs)
        l.sort()
        super().__init__(l)

    def __str__(self) -> str:
        return ' '.join(map(str, self))

    @property
    def bonus(self) -> int:
        """Returns the bonus won with the cards in hand (excl. stuk).
        """
        from rules.util import cards_by_suit

        if len(self) == 4 and len(set(card.rank for card in self)) == 1:
            # Four matching ranks = 100.
            return 100
        elif len(self) >= 3:
            # Look for sequences.
            for suit_cards in cards_by_suit(self).values():
                if len(suit_cards) < 3:
                    continue
                suit_ranks = set(card.rank for card in suit_cards)
                for series in BONUS_SERIES_TABLE:
                    if len(series & suit_ranks) == len(series):
                        # Sequence: 3-in-a-row -> 20, 4 -> 50.
                        return {3: 20, 4: 50}[len(series)]
        else:
            # Nothing.
            return 0

    def stuk(self, trump: int) -> int:
        return (20
                if Card(trump, QUEEN) in self
                and Card(trump, KING) in self
                else 0)


class Table(List[Card]):
    def __init__(self, trump: int, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.trump = trump
        self.winning_index = -1
        self.winning_total_rank = -1

    @property
    def asked(self) -> int:
        return self[0].suit

    @property
    def bonus(self) -> int:
        """Returns the bonus won with the cards on the table (ex. stuk).
        """
        from rules.util import cards_by_suit

        if len(self) == 4 and len(set(card.rank for card in self)) == 1:
            # Four matching ranks = 100.
            return 100
        elif len(self) >= 3:
            # Look for sequences.
            for suit_cards in cards_by_suit(self).values():
                if len(suit_cards) < 3:
                    continue
                suit_ranks = set(card.rank for card in suit_cards)
                for series in BONUS_SERIES_TABLE:
                    if len(series & suit_ranks) == len(series):
                        # Sequence: 3-in-a-row -> 20, 4 -> 50.
                        return {3: 20, 4: 50}[len(series)]
        else:
            # Nothing.
            return 0

    @property
    def stuk(self) -> int:
        return (20
                if Card(self.trump, QUEEN) in self
                and Card(self.trump, KING) in self
                else 0)

    @property
    def value(self) -> int:
        return sum(card.trump_value
                   if card.suit == self.trump
                   else card.value
                   for card in self)

    @property
    def winning_card(self) -> Card:
        return self[self.winning_index]

    @property
    def winning_team(self) -> int:
        """Returns 0 if winning team is US, 1 if winning team is THEM.
        """
        return (len(self) + self.winning_index) % 2

    def append(self, card: Card) -> None:
        # Append first. Then `self.asked` is guaranteed.
        super().append(card)
        r = self.card_total_rank(card)
        if r > self.winning_total_rank:
            self.winning_index = len(self) - 1
            self.winning_total_rank = r

    def card_total_rank(self, card: Card) -> int:
        """Returns a card rank that orders trump over asked over other.
        """
        if card.suit == self.trump:
            return 2 * 8 + card.trump_rank
        elif card.suit == self.asked:
            return 8 + card.rank
        else:
            return card.rank
