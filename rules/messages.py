
class Message:
    pass


class GameCreated(Message):
    pass


class PlayerJoinsGame(Message):
    pass


class PlayerLeavesGame(Message):
    pass


class GameReady(Message):
    pass


class StartGame(Message):
    pass


class StartHand(Message):
    pass


class DealCards(Message):
    pass


# ...

# Player


class ReceiveCards(Message):
    pass

