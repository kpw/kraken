from __future__ import annotations
from collections import defaultdict
from typing import Dict, Iterable, List, TYPE_CHECKING

if TYPE_CHECKING:
    from rules.models import Card


def cards_by_suit(cards: Iterable[Card]) -> Dict[int, List[Card]]:
    """Returns the cards grouped by suit.
    """
    by_suit = defaultdict(list)
    for card in cards:
        by_suit[card.suit].append(card)
    return by_suit
