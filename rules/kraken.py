from random import shuffle
from typing import Tuple

from rules.models import Card, Hand, Table, US, ALL_CARDS
from rules.util import cards_by_suit


def deal() -> Tuple[Hand, Hand, Hand, Hand]:
    deck = list(ALL_CARDS)
    shuffle(deck)

    return (Hand(deck[0::4]),
            Hand(deck[1::4]),
            Hand(deck[2::4]),
            Hand(deck[3::4]))


def valid(card: Card, hand: Hand, table: Table) -> bool:
    """Returns true if the player can play `card`. (Amsterdam rules.)
    """
    if card not in hand:
        return False
    if len(table) == 0:
        return True

    asked = table.asked
    trump = table.trump

    hand_by_suit = cards_by_suit(hand)

    if card.suit == asked:
        if asked == trump:
            # When trump is asked, I must play higher rank (unless I can't).
            top_rank_table = table.winning_card.trump_rank
            top_rank_hand = max(card.trump_rank
                                for card in hand_by_suit[trump])
            return (card.trump_rank > top_rank_table
                    or top_rank_hand < top_rank_table)
        # I'm simply following suit. That's fine.
        return True

    if len(hand_by_suit[asked]):
        # Whenever I can, I must follow suit.
        return False

    if card.suit == trump:
        if table.card_total_rank(card) > table.winning_total_rank:
            # My trump is higher than all other cards. Good.
            return True
        # Under-trumping is forbidden unless I have no other choice.
        if len(hand_by_suit[trump]) == len(hand):
            top_rank_table = table.winning_card.trump_rank
            top_rank_hand = max(card.trump_rank
                                for card in hand_by_suit[trump])
            return top_rank_hand < top_rank_table
        # I hold a non-trump card. Should've played that.
        return False

    if len(hand_by_suit[trump]):
        # It's fine _not_ to play a trump card if and only if my mate is
        # winning or I have no trump cards.
        return table.winning_team == US

    return True
